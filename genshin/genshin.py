import math
def Splits(num_d, num_k):
    if num_d == 0:
        return num_k
    elif num_k == 0:
        return num_d
    else:
        return math.gcd(num_d, num_k)

def SplitWood(string):
    num_d = 0
    num_k = 0
    for i in string:
        if i == 'D':
            num_d += 1
            print(Splits(num_d, num_k))
        elif i == 'K':
            num_k += 1
            print(Splits(num_d, num_k))
        else:
            print("Check inputs")
    return

#Example usage:
SplitWood("DKDKDKDD")